package main.java.qa.names;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by lpirnie on 04/07/2019.
 */
public class NamePrinter {
    public static void main(String[] args) {
        try {
            Scanner input = new Scanner(new File("names.txt"));
            while (input.hasNext()) {
                System.out.println(input.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
